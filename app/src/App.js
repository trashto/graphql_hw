import './App.css';
import MessageList from './components/MessageList'
import MessageInput from './components/MessageInput'

function App() {
  return (
    <div className="App">
      <MessageList />
      <MessageInput />
    </div>
  );
}

export default App;


// TODO add event SEND, LIKE, DISLIKE, REPLY 
// and LIKE, DISLIKE on server