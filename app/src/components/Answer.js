import React from 'react'
import { useState } from 'react'

const Answer = props => {
  const { text, likes, dislikes } = props;

  const [likeCount, increaseCount] = useState(likes ?? 0);
  const [dislikeCount, decreaseCount] = useState(dislikes ?? 0);

  return (
    <div className="answer">
      <span>{text}</span>
      <div className="answer-state">
        <div className="likes">
          <i className="fas fa-thumbs-up" onClick={() => increaseCount(likeCount + 1)}></i>
          <span>{likeCount}</span>
        </div>
        <div className="dislikes">
          <i className="fas fa-thumbs-down" onClick={() => decreaseCount(dislikeCount + 1)}></i>
          <span>{dislikeCount}</span>
        </div>
      </div>
    </div>
  )
}

export default Answer