import React, { useState } from "react";
import { Mutation } from 'react-apollo';
import { MESSAGE_QUERY, POST_ANSWER } from '../queries';

const AnswerForm = props => {
  const { messageId, toggleForm } = props
  const [text, setText] = useState('');

  const _updateStore = (store, newAnswer, messageId) => {
    const data = store.readQuery({
      query: MESSAGE_QUERY
    });
    const answeredMessage = data.messages.find(message => message.id === messageId);
    answeredMessage.answers.push(newAnswer);

    store.writeQuery({
      query: MESSAGE_QUERY,
      data
    });
    toggleForm(false);
  }
  return (
    <div className="reply-form">
      <input type="text" onChange={e => setText(e.target.value)}></input>
      <Mutation
        mutation={POST_ANSWER}
        variables={{ messageId, text }}

        update={(store, { data: { postAnswer } }) => {
          _updateStore(store, postAnswer, messageId)
        }}
      >
        {postAnswer =>
          <button className="send-answer" onClick={postAnswer}>Send</button>
        }

      </Mutation>
    </div>
  )
}
export default AnswerForm