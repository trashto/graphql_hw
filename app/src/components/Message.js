import React from 'react'
import Answer from './Answer'
import { useState } from 'react'
import AnswerForm from './AnswerForm'

const Message = props => {
  const { id, text, answers, likes, dislikes } = props;

  const [likeCount, increaseCount] = useState(likes ?? 0);
  const [dislikeCount, decreaseCount] = useState(dislikes ?? 0);

  const [showAnswerForm, toggleForm] = useState(false);

  return (
    <div className="message" id={id}>
      <div className="message-state">
        <div className="likes ">
          <i className="fas fa-thumbs-up" onClick={() => increaseCount(likeCount + 1)}></i>
          <span>{likeCount}</span>
        </div>
        <div className="dislikes">
          <i className="fas fa-thumbs-down" onClick={() => decreaseCount(dislikeCount + 1)}></i>
          <span>{dislikeCount}</span>
        </div>
      </div>
      <p>{text}</p>

      {answers.map(answer => <Answer key={answer.id} {...answer} />)}

      <button className="reply" onClick={() => toggleForm(!showAnswerForm)}>{showAnswerForm ? "Close" : "Reply"}</button>
      {showAnswerForm && <AnswerForm messageId={id} toggleForm={toggleForm} />}
    </div>
  )
}



export default Message