import React from 'react'
import { Mutation } from 'react-apollo';
import { MESSAGE_QUERY, POST_MESSAGE } from '../queries';
import { useState } from 'react';

const MessageInput = props => {
  const [text, setText] = useState('');

  const _updateStore = (store, newMessage) => {
    const data = store.readQuery({
      query: MESSAGE_QUERY
    })

    data.messages.push(newMessage);

    store.writeQuery({
      query: MESSAGE_QUERY,
      data
    });
    setText('');
  }

  return (
    <div className="input-wrapper">
      <input type="text" value={text} onChange={e => setText(e.target.value)}></input>
      <Mutation
        mutation={POST_MESSAGE}
        variables={{ text }}

        update={(store, { data: { postMessage } }) => {
          _updateStore(store, postMessage)
        }}
      >
        {postMessage => <button className="send-btn" onClick={postMessage}>Send</button>}
      </Mutation>
    </div>
  )
}

export default MessageInput