import React from 'react'
import { Query } from 'react-apollo'
import { MESSAGE_QUERY, NEW_MESSAGE_SUBSCRIPTION } from '../queries'
import Message from './Message'

const MessageList = props => {

  const _subscribeToNewMessage = subscribeToMore => {

    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;

        const { newMessage } = subscriptionData.data;
        
        const exist = prev.messages.find(({ id }) => id === newMessage.id);

        
        if (exist) return prev;

        return { ...prev, messages: [...prev.messages, newMessage] }
      }
    })
  }

  return (
    <Query query={MESSAGE_QUERY}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>Error...</div>;
        _subscribeToNewMessage(subscribeToMore)

        const { messages } = data;

        return (
          <div className="messages">

            {messages.map(message => <Message key={message.id}  {...message} />)}
          </div>
        )
      }}
    </Query>
  );
}

export default MessageList