import gql from "graphql-tag";

export const MESSAGE_QUERY = gql`
  query messageQuery{
    messages{
      id text answers{
        id text
      }
    }
  }
`;

export const ANSWER_QUERY = gql`
  query answerQuery{
     answers{
        id text
      }
    }
`;



export const POST_MESSAGE = gql`
  mutation postMutation($text: String!){
    postMessage(text: $text){
      id text
    }
  }
`;

export const POST_ANSWER = gql`
  mutation postMutation($messageId: ID!, $text: String!){
    postAnswer(messageId: $messageId, text: $text){
      id text
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage{
      id 
      text
      answers{
        id 
        text
      }
    }
  }
`
export const NEW_ANSWER_SUBSCRIPTION = gql`
  subscription {
    newAnswer{
      id 
      text
    }
  }
`