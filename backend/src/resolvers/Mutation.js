function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    text: args.text
  })
}


async function postAnswer(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId
  });
  
  if (!messageExists) {
    throw new Error(`message with ID ${args.messageId} does not exist`)
  }

  return context.prisma.createAnswer({
    message: { connect: { id: args.messageId } },
    text: args.text,
  });
}

module.exports = {
  postMessage,
  postAnswer
}