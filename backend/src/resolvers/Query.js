async function messages(parent, args, context) {
  const messages = await context.prisma.messages()
  return messages;
}

async function answers(parent, args, context) {
  const answers = await context.prisma.answers()
  return answers;
}    

module.exports = {
  messages,
  answers
}