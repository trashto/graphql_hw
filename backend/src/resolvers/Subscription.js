function newMassageSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe.message({
    mutation_in: ['CREATED']
  }).node();
}

function newAnswerSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe.answer({
    mutation_in: ['CREATED']
  }).node();
}

const newMessage = {
  subscribe: newMassageSubscribe,
  resolve: payload => {
    return payload;
  }
};

const newAnswer = {
  subscribe: newAnswerSubscribe,
  resolve: payload => {
    return payload;
  }
}

module.exports = {
  newMessage,
  newAnswer,
  
}